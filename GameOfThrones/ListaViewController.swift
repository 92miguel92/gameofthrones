//
//  ListaViewController.swift
//  GameOfThrones
//
//  Created by Miguel Ángel Jareño Escudero on 31/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit

class ListaViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var miDS : GestorTabla!
    var miDel : GestorTabla!
    
    @IBOutlet weak var textInserted: UITextField!
    @IBAction func insertRow(_ sender: UIButton) {
        
        let numRows = miDS.nombres.count
        miDS.insertarCelda(enTabla: tableView, enFila: numRows, conTexto: textInserted.text!)
    }
    @IBAction func modeEdition(_ sender: UIButton) {
        
        if !self.tableView.isEditing{
            self.tableView.setEditing(true, animated: true)
        }
        else{
            self.tableView.setEditing(false, animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        miDS = GestorTabla()
        miDel = GestorTabla()
        tableView.dataSource = miDS
        tableView.delegate = miDel
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
