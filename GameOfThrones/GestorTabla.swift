//
//  GestorTabla.swift
//  GameOfThrones
//
//  Created by Miguel Ángel Jareño Escudero on 31/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit

class GestorTabla: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var nombres = ["Daenerys Targaryen", "Jon Nieve", "Cersei Lannister", "Eddard Stark"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "unaCelda", for: indexPath)
        let cell = UITableViewCell(style: .default, reuseIdentifier: "")
        cell.textLabel?.text = self.nombres[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle{
        
        return UITableViewCellEditingStyle.delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.delete){
            self.nombres.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
    }
    
    func insertarCelda(enTabla: UITableView, enFila: Int, conTexto: String){
        
        self.nombres.insert(conTexto, at:enFila)
        let indexPath = IndexPath(row:enFila, section:0)
        enTabla.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
